/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 12:01:04 by ddombya           #+#    #+#             */
/*   Updated: 2017/11/16 21:54:09 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s)
{
	int		size;
	char	*dest;
	int		i;

	size = 0;
	i = 0;
	while (s[size])
		size++;
	dest = (char*)malloc(sizeof(*dest) * (size + 1));
	if (dest == 0)
		return (NULL);
	while (i < size)
	{
		dest[i] = s[i];
		i++;
	}
	dest[size] = '\0';
	return (dest);
}
